<?php

include 'DataBase.php';

class DAO
{

	private $connection;

	function DAO()
	{
		$DAO = new DataBase();
		$this->connection = $DAO->connect();
	}
	/**	
	 * 
	 * @param String $table Get table 
	 * @param Array $values Get data to insert
	 * 
	 * @return Integer 
	 */
	function insert($table, $values = array())
	{
		$valuesD = "";
		foreach ($values as $value) {
			$valuesD .= ((!is_null($value)) ? "'" . $value . "', " : "NULL, ");
		}

		$valuesD = substr($valuesD, 0, -2);
		$sentencia = $this->connection->prepare("INSERT INTO " . $table . " VALUES (" . $valuesD . ")");
		$sentencia->execute();
		return $this->connection->lastInsertId();
	}
	/**	
	 * 
	 * @param String $table Get table 
	 * @param String $condition Get condition select 
	 * @param String $select Get data required
	 * @param String? $order Get data order
	 * @param String? $limit Get data limit
	 * @param String? $inner Get data composite
	 * 
	 * @return Object 
	 */
	function consult($table, $condition, $select, $order = "", $limit = "", $inner = "")
	{

		$consult = "SELECT " . $select . " FROM " . $table . " " . $inner . " WHERE " . $condition . " ";
		if ($order != "") {
			$consult .= ' ORDER BY ' . $order;
		}
		if ($limit != "") {
			$consult .= ' LIMIT ' . $limit;
		}

		$result = $this->connection->query($consult);
		return $result;
	}
	/**	
	 * 
	 * @param String $table Get table 
	 * @param String $id Get id table
	 * 
	 * @return Array 
	 */
	function getById($tabla, $id)
	{
		$consult = "SELECT * FROM $tabla WHERE id =$id'";
		$result = $this->connection->query($consult);
		return $result;
	}
	/**	
	 * 
	 * @param String $table Get table 
	 * @param Array $fileds Get data to update 
	 * @param String $condition Get condition select 
	 * 
	 * @return Integer 
	 */
	function update($table, $fields = array(), $condition)
	{
		$valuesD = "";
		foreach ($fields as $key => $value) {
			$valuesD .= "$key = " . ((!is_null($value)) ? "'" . $value . "', " : "NULL, ");
		}
		$valuesD = substr($valuesD, 0, -2);
		$sentence = $this->connection->prepare("UPDATE  $table SET " . $valuesD . " WHERE " . $condition);
		$sentence->execute();
		return $sentence->rowCount();
	}
	/**	
	 * 
	 * @param String $table Get table 
	 * @param String $condition Get condition delete 
	 * 
	 */
	function delete($tabla, $condition)
	{
		$delete = "DELETE FROM $tabla WHERE " . $condition;
		$sentence = $this->connection->prepare($delete);
		$sentence->execute();
	}

	function close()
	{ //Close conection data base
		$this->connection = null;
	}
}
