<?php
//Funcion para conectar con la base de datos

class DataBase
{

    private $server;
    private $user;
    private $pass;
    private $nameDB;
    private $connection;

    public function DataBase()
    {
        $this->server = "localhost";
        $this->user = "root";
        $this->pass = "";
        $this->nameDB = "id17612528_pruebatecnica";
        $this->connection = null;
    }

    public function connect()
    {
        if ($this->connection == null) {
            try {
                $this->connection = new PDO("mysql:host=" . $this->server . ";dbname=" . $this->nameDB, $this->user, $this->pass);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }

        return $this->connection;
    }

    function disconnect()
    {
        $this->connection = null;
    }
}
