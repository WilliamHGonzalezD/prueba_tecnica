var pagStart = 0;
var pagFinish = 10;
var totalData = 0;

$(document).ready(function () {
    //Valid Login
    if (!localStorage.getItem("usuario")) {
        location.href = '../logeo.html';
    }
    var page = window.location.pathname.split('/');
    page = page[page.length - 1];
    //Load data or event for page
    if (page == "new.html") {
        //Event ready
        getDataLoad();
        setDateAdmission(new Date());
        validError();
        //Events for action
        newEmployee();
        eventValidIdClient();
    } else {
        //Eventos ready.....
        getEmployees('');
        eventDeleteEmployee();
        localStorage.setItem("employee", 0);
    }

    $('#logout').on('click', function(){
        localStorage.removeItem("usuario");
        location.href = '../logeo.html';
    });
    
    $('#newEmployee').on('click', function(){
        localStorage.setItem("employee", 0);
    });
});
/**
 * 
 * @param {Input for generate filter in the table} search 
 * @success {Hidden edit and delete button and data loading in the table}
 */
function getEmployees(search) {
    //Comienza peticion via ajax..
    jQuery.ajax({
        url: '../../src/controllers/EmployeeController.php',
        data: { accion: 1, pagStart: pagStart, pagFinish: pagFinish, search: search },
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            $('#employees').html("");
            totalData = data.count;
            $("#editEmployee").removeAttr('href');
            $("#editEmployee").attr('hidden', true);
            $("#deleteEmployee").removeAttr('href');
            $("#deleteEmployee").attr('hidden', true);
            data.employee.forEach(element => {
                $('#employees').append('<tr onclick="eventSelectEmployee($(this))" data-id="' + element.id + '">' +
                    '<th scope="row">' + element.id + '</th>' +
                    '<td>' + element.first_name + ' ' + element.second_name + '</td>' +
                    '<td>' + element.last_name + ' ' + element.second_last_name + '</td>' +
                    '<td>' + element.country.name + '</td>' +
                    '<td>' + element.type_document.name + '</td>' +
                    '<td>' + element.number_document + '</td>' +
                    '<td>' + element.email + '</td>' +
                    '<td>' + element.area.name + '</td>' +
                    '<td>' + ((element.status == 1) ? 'ACTIVO' : 'INACTIVO') + '</td>' +
                    '<td>' + element.date_admission + '</td>' +
                    '<td>' + element.date_register + '</td>' +
                    '<td>' + ((element.date_update) ? element.date_update : '') + '</td>' +
                    '</tr>');
            });

            if (pagStart > 0) {
                $('#prev').removeClass('disabled');
            } else {
                $('#prev').addClass('disabled');
            }

            if (pagFinish < totalData) {
                $('#next').removeClass('disabled');
            } else {
                $('#next').addClass('disabled');
            }
        },
        error: function (xhr, status) {
            alert('Error al traer los clientes');
        }

    });
    //Termina peticion via ajax..
}
/**
 * 
 * @success {Load data into input select and employee data if you are editing}
 */
function getDataLoad() {
    //Comienza peticion via ajax..
    jQuery.ajax({
        url: '../../src/controllers/EmployeeController.php',
        data: { accion: 2 },
        type: 'POST',
        dataType: 'json',
        success: function (data) {

            area = data[0];
            country = data[1];
            typeDocument = data[2];

            area.forEach(element => {
                $('#area').append('<option value="' + element.id + '">' + element.name + '</option>');
            });

            country.forEach(element => {
                $('#country').append('<option value="' + element.id + '">' + element.name + '</option>');
            });

            typeDocument.forEach(element => {
                $('#type_document').append('<option value="' + element.id + '">' + element.name + '</option>');
            });

            if (localStorage.getItem("employee") > 0) {
                setEditEmployee(localStorage.getItem("employee"));
            }
        },
        error: function (xhr, status) {
            alert('Error al traer los clientes');
        }

    });
    //Termina peticion via ajax..
}
/**
 * 
 * @action {Active validation form}
 */
function newEmployee() {
    $('#new_employee').on('click', function () {
        $('#form-new-employee').addClass('was-validated');
    });
}
/**
 * 
 *@param {Date submitted for restriction entry admission date} d 
 */
function setDateAdmission(d) {
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var strDate = d.getFullYear() + "-" +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day;
    d.setMonth(d.getMonth() - 1);
    var month = d.getMonth() + 1;
    var fnsDate = d.getFullYear() + "-" +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day;
    $('#date_admission').attr('max', strDate);
    $('#date_admission').attr('min', fnsDate);
}
/**
 * 
 * @action {If form is invalid, show alert}
 */
function validError() {
    var queryString = window.location.search;
    var urlParams = new URLSearchParams(queryString);
    var error = urlParams.get('error');

    if (error == 1) {
        $('#errorAlert').text('Los campos del formulario no son validos!');
        $('#errorAlert').removeAttr('hidden');
    }
}
/**
 * 
 * @action {Activate ajax to validate the employee id when updating the document type field or document number field}
 */
function eventValidIdClient() {
    $('#type_document').on('change', function () {
        if ($(this).val().length > 0 && $('#number_document').val().length > 0) {
            validIdClient($(this).val(), $('#number_document').val());
        }
    });

    $('#number_document').on('change', function () {
        if ($(this).val().length > 0 && $('#type_document').val().length > 0) {
            validIdClient($('#type_document').val(), $(this).val());
        }
    });
}
/**
 * 
 * @param {Type Document} typeId 
 * @param {Number Document} typeId 
 * @action {Validate the employee id}
 */
function validIdClient(typeId, id) {
    //Comienza peticion via ajax..
    jQuery.ajax({
        url: '../../src/controllers/EmployeeController.php',
        data: { accion: 4, typeId: typeId, id: id },
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            if (data[0]) {
                $('#type_document').val('');
                $('#errorAlert').text('El empleado identificado con ' + data[0].type_document.name + ' #' + data[0].number_document + ' ya se encuentra registrado!');
                $('#errorAlert').removeAttr('hidden');
            } else {
                $('#errorAlert').text('');
                $('#errorAlert').attr('hidden');
            }
        },
        error: function (xhr, status) {
            alert('Error al traer los clientes');
        }

    });
    //Termina peticion via ajax..
}
/**
 * 
 * @param {Tr} thisTr 
 * @action {Activate the edit button and the delete button}
 */
function eventSelectEmployee(thisTr) {
    $('#employees').children('tr').each(function () {
        $(this).removeClass('table-primary');
    });

    thisTr.addClass('table-primary');

    localStorage.setItem("employee", thisTr.attr('data-id'));
    $("#editEmployee").attr('href', 'new.html');
    $("#editEmployee").removeAttr('hidden');
    $("#deleteEmployee").removeAttr('hidden');
}
/**
 * 
 * @param {Id of the employee to editing} idEmployee 
 * @action {Loading employee data and loading log data}
 */
function setEditEmployee(idEmployee) {
    $('title').text('Editar Empleado');
    $('#new_employee').text('Editar Empleado');
    $('#accion').val(6);
    $('#id').val(idEmployee);

    //Comienza peticion via ajax..
    jQuery.ajax({
        url: '../../src/controllers/EmployeeController.php',
        data: { accion: 5, idEmployee: idEmployee },
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            employee = data.employee[0];
            log = data.log;

            if (employee) {
                $('#first_name').val(employee.first_name);
                $('#second_name').val(employee.second_name);
                $('#last_name').val(employee.last_name);
                $('#second_last_name').val(employee.second_last_name);
                $('#country').val(employee.country.id);
                $('#area').val(employee.area.id);
                $('#type_document').val(employee.type_document.id);
                $('#number_document').val(employee.number_document);
                $('#date_admission').val(employee.date_admission);
                $('#status').val(employee.status);
                $('#status').parent().removeAttr('hidden');
                $('#email').val(employee.email);
                $('#date_register').val(employee.date_register);
                $('#date_register').parent().parent().removeAttr('hidden');
                date = employee.date_register.split(' ')[0].split('/');

                setDateAdmission(new Date(date[2], date[1] - 1, date[0]));
            }

            $('#log').html("");
            log.forEach(element => {
                $('#log').append('<tr>' +
                    '<th scope="row">' + element.id + '</th>' +
                    '<td>' + element.log + '</td>' +
                    '<td>' + element.date_execute + '</td>' +
                    '</tr>');
            });
        },
        error: function (xhr, status) {
            alert('Error al traer los clientes');
        }

    });
    //Termina peticion via ajax..
}
/**
 * 
 * @action {Confirm and remove employee}
 */
function eventDeleteEmployee() {
    $('#deleteEmployee').on('click', function () {
        if (confirm('Está seguro de que desea eliminar el empleado?')) {

            idEmployee = localStorage.getItem("employee");
            //Comienza peticion via ajax..
            jQuery.ajax({
                url: '../../src/controllers/EmployeeController.php',
                data: { accion: 7, idEmployee: idEmployee },
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    location.reload();
                },
                error: function (xhr, status) {
                    alert('Error al traer los clientes');
                }

            });
            //Termina peticion via ajax..
        }
    });
}
