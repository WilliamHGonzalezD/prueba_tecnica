$(document).ready(function () {
    //Event for validations.....
});
/**
 * 
 * @param {Input to validate for regex} vthis
 * @param {Regex to validate} regex
 * @param {Text to disply error} textReg
 */
function validationRegexRequired(vthis, regex, textReg) {
    var reg = regex;
    var vid = vthis.attr('id') + '_invalid';

    if (vthis.is('[required]') && vthis.val().length <= 0) {
        $('#' + vid).html('Campo Requerido');
        vthis.addClass('is-invalid');
    } else if (!reg.test(vthis.val()) && vthis.val().length > 0) {
        $('#' + vid).html(textReg);
        vthis.addClass('is-invalid');
    } else {
        vthis.removeClass('is-invalid');
    }
}