$(document).ready(function () {
	//Event for action
	eventoAccess();
	//Send page index if user is loaded
	if(localStorage.getItem("usuario")){
		location.href = '../view/Employee/index.html';
	}
});
/**
 * 
 * @action {Validate and start session}
 */
function eventoAccess() {
	//Comienza peticion via ajax..
	$('#access').on('click', function () {

		var username = $('#username').val();
		var password = $('#pwd').val();

		if (username.length > 0 && password.length > 0) {

			jQuery.ajax({
				url: '../src/controllers/UserController.php',
				data: { accion: 1, username: username, pass: password },
				type: 'POST',
				dataType: 'json',
				success: function (dataAccess) {

					if (dataAccess.access == 1) {
						alert('Acceso Denegado');
					} else {
						localStorage.setItem("usuario", JSON.stringify(dataAccess));
						location.href = '../view/Employee/index.html';
					}
					//Se muestran los catalogos en el index
				},
				error: function (xhr, status) {
					alert('Usuario y Contraseña no Coinciden');
				}

			});

		} else {
			alert('Información Incompleta');
		}
	});
	//Termina peticion via ajax..
}