<?php
include_once "../models/ImplementEmployee.php";
include_once "../models/ImplementLog.php";
//1. Get action to execute
$data = $_POST['accion'];
//2. Valid if the action is activated
switch ($data) {
    case 1:
        getEmployee($_POST['pagStart'], $_POST['pagFinish'], $_POST['search']);
        break;
    case 2:
        getDataLoad();
        break;
    case 3:
        $data = array(
            'first_name' => ($_POST['first_name']) ? $_POST['first_name'] : FALSE,
            'second_name' => (strlen($_POST['second_name']) > 50) ? FALSE : $_POST['second_name'],
            'last_name' => (trim($_POST['last_name'])) ? trim($_POST['last_name']) : FALSE,
            'second_last_name' => ($_POST['second_last_name']) ? $_POST['second_last_name'] : FALSE,
            'country' => ($_POST['country']) ? $_POST['country'] : FALSE,
            'area' => ($_POST['area']) ? $_POST['area'] : FALSE,
            'type_document' => ($_POST['type_document']) ? $_POST['type_document'] : FALSE,
            'number_document' => ($_POST['number_document']) ? $_POST['number_document'] : FALSE,
            'date_admission' => ($_POST['date_admission']) ? $_POST['date_admission'] : FALSE,
        );

        if (in_array(FALSE, $data, true)) {
            header('Location: ../../view/Employee/new.html?error=1');
        } else {
            $status = newEmployee($data);
            if (!$status) {
                header('Location: ../../view/Employee/new.html?error=1');
            } else {
                header('Location: ../../view/Employee/index.html');
            }
        }

        break;
    case 4:
        $typeId = $_POST['typeId'];
        $id = $_POST['id'];

        getValidEmployee($typeId, $id);
        break;
    case 5:
        $idEmployee = $_POST['idEmployee'];

        getEmployeeById($idEmployee);
        break;
    case 6:
        $data = array(
            'first_name' => ($_POST['first_name']) ? $_POST['first_name'] : FALSE,
            'second_name' => (strlen($_POST['second_name']) > 50) ? FALSE : $_POST['second_name'],
            'last_name' => (trim($_POST['last_name'])) ? trim($_POST['last_name']) : FALSE,
            'second_last_name' => ($_POST['second_last_name']) ? $_POST['second_last_name'] : FALSE,
            'country' => ($_POST['country']) ? $_POST['country'] : FALSE,
            'area' => ($_POST['area']) ? $_POST['area'] : FALSE,
            'type_document' => ($_POST['type_document']) ? $_POST['type_document'] : FALSE,
            'number_document' => ($_POST['number_document']) ? $_POST['number_document'] : FALSE,
            'date_admission' => ($_POST['date_admission']) ? $_POST['date_admission'] : FALSE,
            'id' => ($_POST['id']) ? $_POST['id'] : FALSE,
            'status' => 1,
        );

        if (in_array(FALSE, $data, true)) {
            header('Location: ../../view/Employee/new.html?error=1');
        } else {
            $status = setEmployee($data);

            if (!$status) {
                header('Location: ../../view/Employee/new.html?error=1');
            } else {
                header('Location: ../../view/Employee/index.html');
            }
        }

        break;
    case 7:
        $idEmployee = $_POST['idEmployee'];

        getDeleteEmployee($idEmployee);
        break;
    default:
        echo "No se ha hecho ninguna peticion";
        break;
}
/**
 * Accion = 1
 * 
 * @param integer $pagStart  Index range for get list employee
 * @param integer $pagFinish Finish range for get list employee
 * @param string $search Input for filter data
 * 
 * @return Json return employee data and total records 
 */ 
function getEmployee($pagStart, $pagFinish, $search = "")
{
    //1.. Se consulta la tabla DAO y se guarda en una variable
    $employeeImple = new ImplementEmployee();
    $employees = $employeeImple->getAll($pagStart, $pagFinish, $search);
    $countEmployee = $employeeImple->getCountAll($search);

    //2. Se pasa a JSON para enviarla de nuevo al servidor..
    header('Content-type: application/json; charset=utf-8');

    echo json_encode(
        array(
            'employee' => $employees,
            'count' => $countEmployee
        )
    );
    die();
}
/**
 * Accion = 2
 * 
 * @return Json return area data, country data and type document data 
 */ 
function getDataLoad()
{
    //1.. Se consulta la tabla DAO y se guarda en una variable
    $areaImple = new ImplementArea();
    $countryImple = new ImplementCountry();
    $typeDocumentImple = new ImplementTypeDocument();
    $areas = $areaImple->getAll();
    $countries = $countryImple->getAll();
    $typeDocuments = $typeDocumentImple->getAll();

    //2. Se pasa a JSON para enviarla de nuevo al servidor..
    header('Content-type: application/json; charset=utf-8');

    echo json_encode(array($areas, $countries, $typeDocuments));
    die();
}
/**
 * Accion = 3
 * 
 * @param array $data Get employee data to create
 * 
 * @return Boolean TRUE or FALSE
 */ 
function newEmployee($data)
{
    //1.. Se envían los datos del cliente
    $employeeImple = new ImplementEmployee();
    return $employeeImple->newEmployee($data);
}
/**
 * Accion = 4
 * 
 * @param Integer $typeId Get type document to validate
 * @param String $id Get number document to validate
 * 
 * @return Employee
 */ 
function getValidEmployee($typeId, $id)
{
    //1.. Se consulta la tabla DAO
    $employeeImple = new ImplementEmployee();
    $employee = $employeeImple->getValidEmployee($typeId, $id);

    //2. Se pasa a JSON para enviarla de nuevo al front..
    header('Content-type: application/json; charset=utf-8');

    echo json_encode($employee);
    die();
}
/**
 * Accion = 5
 * 
 * @param Integer $idEmployee Get id register employee
 * 
 * @return json Get data employee and data log
 */ 
function getEmployeeById($idEmployee)
{
    //1.. Se consulta la tabla DAO y se guarda en una variable
    $employeeImple = new ImplementEmployee();
    $logImple = new ImplementLog();
    $employee = $employeeImple->getEmployeeById($idEmployee);
    $log = $logImple->getByEmployeeId($idEmployee);

    //2. Se pasa a JSON para enviarla de nuevo al servidor..
    header('Content-type: application/json; charset=utf-8');

    echo json_encode(
        array(
            'employee' => $employee,
            'log' => $log
        )
    );
    die();
}
/**
 * Accion = 6
 * 
 * @param Array $data Get data employee to upload
 * 
 * @return Boolean
 */ 
function setEmployee($data)
{
    //1.. Se envían los datos del cliente
    $employeeImple = new ImplementEmployee();
    return $employeeImple->setEmployee($data);
}
/**
 * Accion = 7
 * 
 * @param Integer $id Get id register employee to delete
 * 
 * @return Boolean
 */ 
function getDeleteEmployee($id)
{
    //1.. Se envían los datos del cliente
    $employeeImple = new ImplementEmployee();
    $employeeImple->delEmployee($id);

    //2. Se pasa a JSON para enviarla de nuevo al servidor..
    header('Content-type: application/json; charset=utf-8');

    echo json_encode(true);
    die();
}
