<?php

//Se reciben todas las peticiones ajax y se responden
//Llamada del core del negocio
include_once "../models/ImplementUser.php";
//1..Recibe la peticion de ajax
$data = $_POST['accion'];
//2..Revisa que caso es
switch ($data) {
    case 1:
        validateAccess($_POST['username'], $_POST['pass']);
        break;
    default:
        echo "No se ha hecho ninguna peticion";
        break;
}

//3..Funciones que llamaran las funciones del DAO y devolveran los datos al ajax
function validateAccess($username, $pass)
{
    //1.. Se consulta la tabla DAO y se guarda en una variable
    $usuarioImple   = new ImplementUser();
    $acceso        = $usuarioImple->validarUser($username, $pass);

    if (!is_array($acceso) && $acceso === false) {
        $acceso['access'] = 1;
    }
    //2. Se pasa a JSON para enviarla de nuevo al servidor..
    header('Content-type: application/json; charset=utf-8');

    echo json_encode($acceso);
    die();
}
