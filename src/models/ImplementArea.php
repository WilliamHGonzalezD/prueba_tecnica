<?php

include_once '../../config/DAO.php';
include_once 'db_object/Area.php';

/**
 * Description of Profile
 *
 * @author William Gonzalez
 */
class ImplementArea
{

	private $area;

	function ImplementArea()
	{
		$this->area = new Area();
	}

	function getAreaById($id)
	{

		$DAO = new DAO();
		$query = $DAO->consult('area', "id = " . $id, '*');
		$DAO->close();

		if (!empty($query)) {

			$area = 0;
			while ($row = $query->fetch()) {
				$this->area->setId($row['id']);
				$this->area->setName($row['name']);
				$area = $this->area;

				break;
			}
			return get_object_vars($area);
		}

		return 0;
	}

	function getAll()
	{

		$DAO = new DAO();
		$query = $DAO->consult('area', "1", '*');
		$DAO->close();
		
		if (!empty($query)) {

			$areas = array();
			while ($row = $query->fetch()) {
				
				$this->area = new Area();
				$this->area->setId($row['id']);
				$this->area->setName($row['name']);
				$areas[] = get_object_vars($this->area);
			}
			return $areas;
		}

		return array();
	}
}
