<?php

include_once '../../config/DAO.php';
include_once 'db_object/Country.php';

/**
 * Description of Profile
 *
 * @author William Gonzalez
 */
class ImplementCountry
{

	private $profile;

	function ImplementCountry()
	{
		$this->country = new Country();
	}

	function getCountryById($id)
	{

		$DAO = new DAO();
		$query = $DAO->consult('country', "id = " . $id, '*');
		$DAO->close();

		if (!empty($query)) {

			$country = 0;
			while ($row = $query->fetch()) {
				$this->country->setId($row['id']);
				$this->country->setName($row['name']);
				$country = $this->country;

				break;
			}
			return get_object_vars($country);
		}

		return 0;
	}

	function getAll()
	{

		$DAO = new DAO();
		$query = $DAO->consult('country', "1", '*');
		$DAO->close();
		
		if (!empty($query)) {

			$countries = array();
			while ($row = $query->fetch()) {
				
				$this->country = new Country();
				$this->country->setId($row['id']);
				$this->country->setName($row['name']);
				$countries[] = get_object_vars($this->country);
			}
			return $countries;
		}

		return array();
	}
}
