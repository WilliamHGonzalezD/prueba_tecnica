<?php

include_once '../../config/DAO.php';
include_once 'db_object/Employee.php';
include_once 'ImplementCountry.php';
include_once 'ImplementTypeDocument.php';
include_once 'ImplementArea.php';

/**
 * Description of Employee
 *
 * @author William Gonzalez
 */
class ImplementEmployee
{

	private $employee;

	function ImplementEmployee()
	{
		$this->employee = new Employee();
	}
	/**
	 * 
	 * @param integer $pagStart  Index range for get list employee
	 * @param integer $pagFinish Finish range for get list employee
	 * @param string $search Input for filter data
	 * 
	 * @return Array return employee data and total records 
	 */
	function getAll($pagStart, $pagFinish, $search = "")
	{
		if (!empty($search)) {
			$where = "first_name LIKE '%$search%'" .
				" OR second_name LIKE '%$search%'" .
				" OR last_name LIKE '%$search%'" .
				" OR second_last_name LIKE '%$search%'" .
				" OR (select name from type_document where id = type_document_id) LIKE '%$search%'" .
				" OR (select name from country where id = country_id) LIKE '%$search%'" .
				" OR number_document LIKE '%$search%'" .
				" OR email LIKE '%$search%'" .
				" OR IF(status = 1, \"ACTIVO\", \"INACTIVO\") LIKE '%$search%'";
		} else {
			$where = "1";
		}

		$DAO = new DAO();
		$query = $DAO->consult('employee', $where, '*', "", $pagStart . ',' . $pagFinish);
		$DAO->close();

		if (!empty($query)) {

			$employee = array();
			while ($row = $query->fetch()) {

				$countryI = new ImplementCountry();
				$typeDocumentI = new ImplementTypeDocument();
				$areaI = new ImplementArea();

				$this->employee = new Employee();
				$this->employee->setId($row['id']);
				$this->employee->setLastName($row['last_name']);
				$this->employee->setSecondLastName($row['second_last_name']);
				$this->employee->setFirstName($row['first_name']);
				$this->employee->setSecondName($row['second_name']);
				$this->employee->setCountry($countryI->getCountryById($row['country_id']));
				$this->employee->setTypeDocument($typeDocumentI->getTypeDocumentById($row['type_document_id']));
				$this->employee->setNumberDocument($row['number_document']);
				$this->employee->setEmail($row['email']);
				$this->employee->setDateAdmission($row['date_admission']);
				$this->employee->setArea($areaI->getAreaById($row['area_id']));
				$this->employee->setStatus($row['status']);
				$this->employee->setDateRegister($row['date_register']);
				$this->employee->setDateUpdate($row['date_update']);
				$employee[] = get_object_vars($this->employee);
			}
			return $employee;
		}

		return array();
	}
	/**
	 * 
	 * @param string $first_name  Get first name for generate email
	 * @param string $last_name Get last name for generate email
	 * @param Integer $country Get country id for generate email
	 * 
	 * @return string 
	 */
	function getEmailValid($first_name, $last_name, $country)
	{
		$row = 0;
		$email = strtolower($first_name) . '.' . strtolower(str_replace(' ', "", $last_name));
		$dom = "@cidenet.com." . (($country == 1) ? 'co' : 'us');

		$DAO = new DAO();
		$query = $DAO->consult('employee', "email like '$email%' and country_id = $country", '*');
		$DAO->close();

		if ($query->rowCount() > 0) {
			$row = $query->rowCount();
			$mailP = $email . ($row == 0) ? "" : '.' . $row;
			
    		$query = $DAO->consult('employee', "email = '$mailP' and country_id = $country", '*');
    		$DAO->close();
    		
    		if ($query->rowCount() > 0) {
    		    $email .= '.' . ($row+1);
    		}else{
    		    $email .= ($row == 0) ? "" : '.' . $row;
    		}    
			
		}

		return $email . $dom;
	}
	/**
	 * 
	 * @param Array $data  Get data employee to create
	 * 
	 * @return string 
	 */
	function newEmployee($data = array())
	{
		$email = $this->getEmailValid($data['first_name'], $data['last_name'], $data['country']);

		$dataInsert = array(
			NULL,
			$data['last_name'],
			$data['second_last_name'],
			$data['first_name'],
			$data['second_name'],
			$data['country'],
			$data['type_document'],
			$data['number_document'],
			$email,
			$data['date_admission'],
			$data['area'],
			1,
			date("Y-m-d H:i:s"),
			NULL
		);
		$DAO = new DAO();
		$id = $DAO->insert('employee', $dataInsert);
		$DAO->close();

		return ($id > 0) ? TRUE : FALSE;
	}
	/**
	 * 
	 * @param Integer $typeId  Get type document employee to validate
	 * @param string $id  Get number document employee to validate
	 * 
	 * @return Employee 
	 */
	function getValidEmployee($typeId, $id)
	{
		$DAO = new DAO();
		$query = $DAO->consult('employee', "type_document_id = '$typeId' AND number_document = '$id'", '*');
		$DAO->close();

		$employee = array();

		if ($query->rowCount() > 0) {
			$row = $query->fetch();
			$countryI = new ImplementCountry();
			$typeDocumentI = new ImplementTypeDocument();
			$areaI = new ImplementArea();

			$this->employee = new Employee();
			$this->employee->setId($row['id']);
			$this->employee->setLastName($row['last_name']);
			$this->employee->setSecondLastName($row['second_last_name']);
			$this->employee->setFirstName($row['first_name']);
			$this->employee->setSecondName($row['second_name']);
			$this->employee->setCountry($countryI->getCountryById($row['country_id']));
			$this->employee->setTypeDocument($typeDocumentI->getTypeDocumentById($row['type_document_id']));
			$this->employee->setNumberDocument($row['number_document']);
			$this->employee->setEmail($row['email']);
			$this->employee->setDateAdmission($row['date_admission']);
			$this->employee->setArea($areaI->getAreaById($row['area_id']));
			$this->employee->setStatus($row['status']);
			$this->employee->setDateRegister($row['date_register']);
			$employee[] = get_object_vars($this->employee);
		}

		return $employee;
	}
	/**
	 * 
	 * @param string $search  Get value to filter data
	 * 
	 * @return Integer 
	 */
	function getCountAll($search)
	{
		if (!empty($search)) {
			$where = "first_name LIKE '%$search%'" .
				" OR second_name LIKE '%$search%'" .
				" OR last_name LIKE '%$search%'" .
				" OR second_last_name LIKE '%$search%'" .
				" OR (select name from type_document where id = type_document_id) LIKE '%$search%'" .
				" OR number_document LIKE '%$search%'" .
				" OR email LIKE '%$search%'" .
				" OR IF(status = 1, \"ACTIVO\", \"INACTIVO\") LIKE '%$search%'";
		} else {
			$where = "1";
		}

		$DAO = new DAO();
		$query = $DAO->consult('employee', $where, '*');
		$DAO->close();

		return $query->rowCount();
	}
	/**
	 * 
	 * @param Integer $id  Get id employee
	 * 
	 * @return Employee 
	 */
	function getEmployeeById($id)
	{
		$DAO = new DAO();
		$query = $DAO->consult('employee', "id = $id", '*');
		$DAO->close();

		if ($query->rowCount() > 0) {
			$employee = array();
			$row = $query->fetch();

			$countryI = new ImplementCountry();
			$typeDocumentI = new ImplementTypeDocument();
			$areaI = new ImplementArea();

			$this->employee = new Employee();
			$this->employee->setId($row['id']);
			$this->employee->setLastName($row['last_name']);
			$this->employee->setSecondLastName($row['second_last_name']);
			$this->employee->setFirstName($row['first_name']);
			$this->employee->setSecondName($row['second_name']);
			$this->employee->setCountry($countryI->getCountryById($row['country_id']));
			$this->employee->setTypeDocument($typeDocumentI->getTypeDocumentById($row['type_document_id']));
			$this->employee->setNumberDocument($row['number_document']);
			$this->employee->setEmail($row['email']);
			$this->employee->setDateAdmission($row['date_admission']);
			$this->employee->setArea($areaI->getAreaById($row['area_id']));
			$this->employee->setStatus($row['status']);
			$this->employee->setDateRegister(date("d/m/Y - H:i:s", strtotime($row['date_register'])));
			$employee[] = get_object_vars($this->employee);
			return $employee;
		}

		return array();
	}
	/**
	 * 
	 * @param Array $data  Get data employee to upload
	 * 
	 * @return Employee 
	 */
	function setEmployee($data = array())
	{
		$email = $this->getEmailValid($data['first_name'], $data['last_name'], $data['country']);

		$dataUpdate = array(
			'last_name' => $data['last_name'],
			'second_last_name' => $data['second_last_name'],
			'first_name' => $data['first_name'],
			'second_name' => $data['second_name'],
			'country_id' => $data['country'],
			'type_document_id' => $data['type_document'],
			'number_document' => $data['number_document'],
			'email' => $email,
			'date_admission' => $data['date_admission'],
			'area_id' => $data['area'],
			'status' => $data['status'],
			'date_update' => date("Y-m-d H:i:s")
		);
		$DAO = new DAO();
		$id = $DAO->update('employee', $dataUpdate, 'id = ' . $data['id']);
		$DAO->close();

		return ($id > 0) ? TRUE : FALSE;
	}
	/**	
	 * 
	 * @param Integer $id  Get id employee to delete
	 * 
	 * @return Boolean 
	 */
	function delEmployee($id)
	{
		$DAO = new DAO();
		$DAO->delete('employee', 'id = ' . $id);
		$DAO->close();

		return TRUE;
	}
}
