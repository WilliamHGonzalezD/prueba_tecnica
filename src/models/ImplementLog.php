<?php

include_once '../../config/DAO.php';
include_once 'db_object/Log.php';

/**
 * Description of CategoriaManager
 *
 * @author William Gonzalez
 */
class ImplementLog
{

	private $log;

	function ImplementLog()
	{
		$this->log = new Log();
	}

	function getByEmployeeId($id)
	{

		$DAO = new DAO();
		$query = $DAO->consult('log', "employee_id = $id", '*');
		$DAO->close();

		if (!empty($query)) {
			$log = array();

			while ($row = $query->fetch()) {
				$this->log = new Log();
				$this->log->setId($row['id']);
				$this->log->setLog($row['log']);
				$this->log->setDateExecute($row['date_execute']);
				$log[] = $this->log;
			}
			return $log;
		}

		return array();
	}
}
