<?php

include_once '../../config/DAO.php';
include_once 'db_object/Profile.php';

/**
 * Description of Profile
 *
 * @author William Gonzalez
 */
class ImplementProfile
{

	private $profile;

	function ImplementProfile()
	{
		$this->profile = new Profile();
	}

	function getProfileById($id)
	{

		$DAO = new DAO();
		$query = $DAO->consult('profile', "id = " . $id, '*');
		$DAO->close();

		if (!empty($query)) {

			$profile = 0;
			while ($row = $query->fetch()) {
				$this->profile->setId($row['id']);
				$this->profile->setName($row['name']);
				$profile = $this->profile;

				break;
			}
			return get_object_vars($profile);
		}

		return 0;
	}
}
