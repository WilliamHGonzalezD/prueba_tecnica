<?php

include_once '../../config/DAO.php';
include_once 'db_object/TypeDocument.php';

/**
 * Description of Profile
 *
 * @author William Gonzalez
 */
class ImplementTypeDocument
{

	private $profile;

	function ImplementTypeDocument()
	{
		$this->type_document = new TypeDocument();
	}

	function getTypeDocumentById($id)
	{

		$DAO = new DAO();
		$query = $DAO->consult('type_document', "id = " . $id, '*');
		$DAO->close();

		if (!empty($query)) {

			$type_document = 0;
			while ($row = $query->fetch()) {
				$this->type_document->setId($row['id']);
				$this->type_document->setName($row['name']);
				$type_document = $this->type_document;

				break;
			}
			return get_object_vars($type_document);
		}

		return 0;
	}

	function getAll()
	{

		$DAO = new DAO();
		$query = $DAO->consult('type_document', "1", '*');
		$DAO->close();
		
		if (!empty($query)) {

			$typeDocuments = array();
			while ($row = $query->fetch()) {
				
				$this->type_document = new TypeDocument();
				$this->type_document->setId($row['id']);
				$this->type_document->setName($row['name']);
				$typeDocuments[] = get_object_vars($this->type_document);
			}
			return $typeDocuments;
		}

		return array();
	}
}
