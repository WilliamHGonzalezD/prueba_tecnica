<?php

include_once '../../config/DAO.php';
include_once 'ImplementProfile.php';
include_once 'db_object/User.php';

/**
 * Description of CategoriaManager
 *
 * @author William Gonzalez
 */
class ImplementUser
{

	private $user;

	function ImplementUser()
	{
		$this->user = new User();
	}

	function validarUser($username, $password)
	{

		$DAO = new DAO();
		$query = $DAO->consult('user', "username = '" . $username . "' AND password = '" . $password . "' AND status = 1", '*');
		$DAO->close();

		if (!empty($query)) {

			$user = 0;

			while ($row = $query->fetch()) {

				$profileI = new ImplementProfile();

				$this->user->setId($row['id']);
				$this->user->setFirstName($row['first_name']);
				$this->user->setLastName($row['last_name']);
				$this->user->setUsername($row['username']);
				$this->user->setStatus($row['status']);
				$this->user->setPassword('');
				$this->user->setProfile($profileI->getProfileById($row['profile_id']));
				$user = $this->user;

				break;
			}
			return get_object_vars($user);
		}

		return 0;
	}
}
