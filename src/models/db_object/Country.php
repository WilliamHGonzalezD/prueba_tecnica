<?php

class Country
{

    public $id;
    public $name;

    function Country()
    {
        $this->id = null;
        $this->name = null;
    }

    //Getter and Setter
    function getId()
    {
        return $this->id;
    }

    function getName()
    {
        return $this->name;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setName($name)
    {
        $this->name = $name;
    }
    //Fin Getter and Setter

}
