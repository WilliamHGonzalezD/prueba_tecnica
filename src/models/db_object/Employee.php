<?php

class Employee
{

    public $id;
    public $last_name;
    public $second_last_name;
    public $first_name;
    public $second_name;
    public $country;
    public $type_document;
    public $number_document;
    public $email;
    public $date_admission;
    public $area;
    public $status;
    public $date_register;
    public $date_update;

    function Employee()
    {
        $this->id = null;
        $this->last_name = null;
        $this->second_last_name = null;
        $this->first_name = null;
        $this->second_name = null;
        $this->country = null;
        $this->type_document = null;
        $this->number_document = null;
        $this->email = null;
        $this->date_admission = null;
        $this->area = null;
        $this->status = null;
        $this->date_register = null;
        $this->date_update = null;
    }

    //Getter and Setter
    function getId()
    {
        return $this->id;
    }

    function getLastName()
    {
        return $this->last_name;
    }
    
    function getSecondLastName()
    {
        return $this->second_last_name;
    }
    
    function getFirstName()
    {
        return $this->first_name;
    }
    
    function getSecondName()
    {
        return $this->second_name;
    }
    
    function getCountry()
    {
        return $this->country;
    }
    
    function getTypeDocument()
    {
        return $this->type_document;
    }
    
    function getNumberDocument()
    {
        return $this->number_document;
    }
    
    function getEmail()
    {
        return $this->email;
    }
    
    function getDateAdmission()
    {
        return $this->date_admission;
    }
    
    function getArea()
    {
        return $this->area;
    }
    
    function getStatus()
    {
        return $this->status;
    }
    
    function getDateRegister()
    {
        return $this->date_register;
    }
    
    function getDateUpdate()
    {
        return $this->date_update;
    }
    
    function setId($id)
    {
        $this->id = $id;
    }

    function setLastName($last_name)
    {
        $this->last_name = $last_name;
    }
    
    function setSecondLastName($second_last_name)
    {
        $this->second_last_name = $second_last_name;
    }
    
    function setFirstName($first_name)
    {
        $this->first_name = $first_name;
    }
    
    function setSecondName($second_name)
    {
        $this->second_name = $second_name;
    }
    
    function setCountry($country)
    {
        $this->country = $country;
    }
    
    function setTypeDocument($type_document)
    {
        $this->type_document = $type_document;
    }
    
    function setNumberDocument($number_document)
    {
        $this->number_document = $number_document;
    }
    
    function setEmail($email)
    {
        $this->email = $email;
    }
    
    function setDateAdmission($date_admission)
    {
        $this->date_admission = $date_admission;
    }
    
    function setArea($area)
    {
        $this->area = $area;
    }
    
    function setStatus($status)
    {
        $this->status = $status;
    }
    
    function setDateRegister($date_register)
    {
        $this->date_register = $date_register;
    }
    
    function setDateUpdate($date_update)
    {
        $this->date_update = $date_update;
    }
    //Fin Getter and Setter

}
