<?php

class Log
{

    public $id;
    public $log;
    public $date_execute;
    public $employee;

    function Profile()
    {
        $this->id = null;
        $this->log = null;
        $this->date_execute = null;
        $this->employee = null;
    }

    //Getter and Setter
    function getId()
    {
        return $this->id;
    }

    function getLog()
    {
        return $this->log;
    }

    function getDateExecute()
    {
        return $this->date_execute;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setLog($log)
    {
        $this->log = $log;
    }

    function setDateExecute($date_execute)
    {
        $this->date_execute = $date_execute;
    }
    //Fin Getter and Setter

}
