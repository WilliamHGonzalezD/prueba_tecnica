<?php

/**
 * Description of Usuarios
 *
 * @author William Gonzalez, Miguel Trujillo, Kevin Aria
 */
class User
{

    public $id;
    public $firstName;
    public $lastName;
    public $username;
    public $password;
    public $status;
    public $profile;

    function User()
    {

        $this->id = null;
        $this->firstName = null;
        $this->lastName = null;
        $this->username = null;
        $this->password = null;
        $this->status = null;
        $this->profile = null;
    }

    //Getter and Setters
    function getId()
    {
        return $this->id;
    }
    
    function getFirstName()
    {
        return $this->firstName;
    }
    
    function getLastName()
    {
        return $this->lastName;
    }
    
    function getUsername()
    {
        return $this->username;
    }
    
    function getPassword()
    {
        return $this->password;
    }
    
    function getStatus()
    {
        return $this->status;
    }
    
    function getProfile()
    {
        return $this->profile;
    }

    function setId($id)
    {
        $this->id = $id;
    }
    
    function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }
    
    function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }
    
    function setUsername($username)
    {
        $this->username = $username;
    }
    
    function setPassword($password)
    {
        $this->password = $password;
    }
    
    function setStatus($status)
    {
        $this->status = $status;
    }
    
    function setProfile($profile)
    {
        $this->profile = $profile;
    }
    //Finish Getter and Setter

}
